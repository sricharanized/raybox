OBJ = main.o
INC = -I "./"

raybox: $(OBJ)
	g++ $(OBJ) -o bin/raybox
	rm -f $(OBJ)

main.o:
	g++ -c main.cpp $(INC)

clean:
	rm -f $(OBJ) bin/raybox