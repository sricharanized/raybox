#ifndef _MAIN_H
#define _MAIN_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <limits>

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "RGB.h"
#include "Vect.h"
#include "Ray.h"
#include "Camera.h"
#include "Color.h"
#include "Light.h"
#include "Objects.h" // root class of all objects in the scene
#include "Sphere.h"
#include "Plane.h"

#define WIDTH 640
#define HEIGHT 480
#define MARGIN 200


#endif
